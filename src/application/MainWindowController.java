package application;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

import javax.naming.TimeLimitExceededException;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class MainWindowController implements Initializable {

	private Image connected;
	private Image disconnected;
	private Communicator communicator;
	
	private HashMap<String, Key> keys = new HashMap<String, Key>(10);

	@FXML
	private TextField RPiAddressTxtField;
	
	@FXML
	private Label connectionStatusLbl;

	@FXML
	private Button forwardBtn;

	@FXML
	private Button downBtn;

	@FXML
	private Button upBtn;
	
	@FXML
	private Button connectionBtn;
	
	@FXML
	private Button rightBtn;
	
	@FXML
	private Button backwardBtn;

	@FXML
	private Button leftBtn;
	
	@FXML
	private Slider lightsPowerSlider;

	/**
	 * Controller of lights power.  
	 * @param event
	 */
	@FXML
	void changeLightsPower(Event event) {
		int power = (int) lightsPowerSlider.getValue();
		String message = "LIGHTS:" + power;
		communicator.sendMessage(message);
		System.out.println(message);
	}
	
	/**
	 * Establish UDP connection with RPi "server" running program 
	 * @param event ActionEvent that triggered the function 
	 */
	@FXML
	void openConnection(ActionEvent event) {
		RPiAddressTxtField.setDisable(true);
		String ip = RPiAddressTxtField.getText();
		if (ip.isEmpty()) {
			communicator = new Communicator();
			RPiAddressTxtField.setText(communicator.getRPiIP());
		} else if (communicator == null && Communicator.isIpValid(ip)) {
			communicator = new Communicator(ip);
		}
		try {
			if (communicator != null && communicator.establishConnection(3000)) {
				connectionStatusLbl.setGraphic(new ImageView(connected));
				connectionBtn.setDisable(true);
				RPiAddressTxtField.setDisable(true);
			} else {
				RPiAddressTxtField.setDisable(false);
			}
		} catch (TimeLimitExceededException e) {
			e.printStackTrace();
			RPiAddressTxtField.setDisable(false);
		}
		validateEditedIP(null);
	}
	
	/**
	 * Connected to RPiAddresTxtField - triggered upon edit and validate entered IP address
	 * @param event
	 */
	@FXML
	void validateEditedIP(Event event) {
		if (Communicator.isIpValid(RPiAddressTxtField.getText())) {
			RPiAddressTxtField.setId("correctIP");
		} else {
			RPiAddressTxtField.setId("wrongIP");
		}
	}
	
	/**
	 * Returns the human readable representation of direction the key (KeyCode) is mapped to
	 * @param code KeyCode of pressed Key
	 * @return String representation of direction associated with pressed key
	 */
	private String keyCodeToDirection(KeyCode code) {
		switch (code.toString()) {
		case "W" : return "UP";
		case "S" : return "DOWN";
		case "UP" : return "FORWARD";
		case "DOWN" : return "BACKWARD";
		default : return code.toString();
		}
	}
	
	/**
	 * Prepares and sends the message about the required movement direction. 
	 * <br>Form of the message is DIRECTION:MOVE where DIRECTION is actual direction and
	 * MOVE is boolean 1/0 according to parameter move
	 * 
	 * @param key representation of pressed key
	 * @param move represents if the movement should start/stop (true/false)
	 */
	private void movement(Key key, boolean move) {
		String message = keyCodeToDirection(key.getKeyCode()) + ":" + (move ? "1" : "0");
		if (move != key.isPressed()) {
			key.manualPressed(move);
			if (communicator.sendMessage(message))
				System.out.println(message);
		} 
	}
	
	@FXML
	void moveUp(Event event) {
		Key key = keys.get(KeyCode.W.toString());
		boolean buttonIsArmed = ((Button)event.getSource()).isArmed(); 
		movement(key, buttonIsArmed);
	}


	@FXML
	void moveDown(Event event) {
		Key key = keys.get(KeyCode.S.toString());
		boolean buttonIsArmed = ((Button)event.getSource()).isArmed(); 
		movement(key, buttonIsArmed);
	}

	@FXML
	void moveLeft(Event event) {
		Key key = keys.get(KeyCode.LEFT.toString());
		boolean buttonIsArmed = ((Button)event.getSource()).isArmed(); 
		movement(key, buttonIsArmed);
	}

	@FXML
	void moveRight(Event event) {
		Key key = keys.get(KeyCode.RIGHT.toString());
		boolean buttonIsArmed = ((Button)event.getSource()).isArmed(); 
		movement(key, buttonIsArmed);
	}

	@FXML
	void moveForward(Event event) {
		Key key = keys.get(KeyCode.UP.toString());
		boolean buttonIsArmed = ((Button)event.getSource()).isArmed(); 
		movement(key, buttonIsArmed);
	}

	@FXML
	void moveBackward(Event event) {
		Key key = keys.get(KeyCode.DOWN.toString());
		boolean buttonIsArmed = ((Button)event.getSource()).isArmed(); 
		movement(key, buttonIsArmed);
	}
	
	public void initializeAccelerators(Scene scene) {
		keys.put(KeyCode.W.toString(), new Key(KeyCode.W, upBtn));
		keys.put(KeyCode.S.toString(), new Key(KeyCode.S, downBtn));
		keys.put(KeyCode.UP.toString(), new Key(KeyCode.UP, forwardBtn));
		keys.put(KeyCode.DOWN.toString(), new Key(KeyCode.DOWN, backwardBtn));
		keys.put(KeyCode.LEFT.toString(), new Key(KeyCode.LEFT, leftBtn));
		keys.put(KeyCode.RIGHT.toString(), new Key(KeyCode.RIGHT, rightBtn));
		
		scene.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				KeyCode code = ((KeyEvent) event).getCode();
				Key key = keys.get(code.toString());
				if (key != null) {
					key.setPressed(true);
				} else if (code == KeyCode.ESCAPE || code == KeyCode.ENTER) {
					lightsPowerSlider.getParent().requestFocus();
				}
			}
		});
		scene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				Key key = keys.get(((KeyEvent) event).getCode().toString());
				if (key != null) {
					key.setPressed(false);
				}
			}
		});
	}
	
	public void onExit() {
		if (communicator!=null && communicator.isConnected()) {
			communicator.closeConnection();
		}
		Platform.exit();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		disconnected = new Image("img/disconnected.png");
		connected = new Image("img/connected.png");
		connectionStatusLbl.setGraphic(new ImageView(disconnected));
		upBtn.setGraphic(new ImageView(new Image("img/up.png")));
		downBtn.setGraphic(new ImageView(new Image("img/down.png")));
		forwardBtn.setGraphic(new ImageView(new Image("img/forward.png")));
		backwardBtn.setGraphic(new ImageView(new Image("img/backward.png")));
		leftBtn.setGraphic(new ImageView(new Image("img/left.png")));
		rightBtn.setGraphic(new ImageView(new Image("img/right.png")));
	}
}
