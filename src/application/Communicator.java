package application;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.regex.Pattern;

import javax.naming.TimeLimitExceededException;

public class Communicator implements Runnable{

	private final String rpiip = "192.168.0.105";
	private InetAddress RPiAddress;
	private InetAddress myIpAddress;
	private static final int PORT = 28050;
	private DatagramSocket socket;
	private DatagramPacket packet;
	private boolean connected = false;
	private Thread stayAliveThread;
	private boolean stayAlive = true;

	/**
	 * Creates default class with preset RPi IP 192.168.0.105
	 * Client settings - uses first found IP (not working properly on Linux) 
	 * starts on port 28050 - nonchangable
	 */
	public Communicator() {
		try {
			myIpAddress = Inet4Address.getLocalHost();
			this.RPiAddress = Inet4Address.getByName(rpiip);
			socket = new DatagramSocket(PORT, myIpAddress);
		} catch (UnknownHostException | SocketException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creates communicator with RPi IP set by parameter
	 * 
	 * @param RPiAddress RPi IP address in correct format form
	 */
	public Communicator(String RPiAddress) {
		try {
			myIpAddress = Inet4Address.getLocalHost();
			this.RPiAddress = Inet4Address.getByName(RPiAddress);
			socket = new DatagramSocket(PORT, myIpAddress);
		} catch (UnknownHostException | SocketException e) {
			e.printStackTrace();
		}
	}

	public boolean sendMessage(String message) {
		packet = new DatagramPacket(message.getBytes(), message.getBytes().length, RPiAddress, PORT);
		
		try {
			socket.send(packet);
			System.out.println("message sent");
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * Establishes connection with RPi using simple authentication and waits for
	 * RPi answer if the connection is established correctly
	 * 
	 * @param timeout time to wait for answer
	 * @return answer whether the connection was established
	 * @throws TimeLimitExceededException
	 */
	public boolean establishConnection(int timeout)	throws TimeLimitExceededException {
		String message = "pi:raspberry@requestingConnection";
		packet = new DatagramPacket(message.getBytes(),
				message.getBytes().length, RPiAddress, PORT);
		byte[] buf = new byte[message.getBytes().length];
		DatagramPacket answer = new DatagramPacket(buf, buf.length);
		try {
			socket.send(packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			socket.setSoTimeout(timeout);
			socket.receive(answer);
			String received = new String(answer.getData());
			received = received.replaceAll("\u0000", "");
			if (received.equalsIgnoreCase("approved")) {
				connected = true;
				stayAliveThread = new Thread(this);
				stayAliveThread.setDaemon(true);
				stayAliveThread.start();
				return true;
			}
			connected = false;
			return false;
		} catch (IOException e1) {
			throw new TimeLimitExceededException("Reqest timeout");
		}
	}
	
	public boolean isConnected() {
		return connected;
	}

	private static final Pattern IPV4_PATTERN = Pattern
			.compile("^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}$");

	/**
	 * Checks if the given IP is valid upon IPv4 format
	 * @param ip String representation of IP
	 * @return true/false
	 */
	public static boolean isIpValid(final String ip) {
		return IPV4_PATTERN.matcher(ip).matches();
	}

	public String getRPiIP() {
		return rpiip;
	}
	
	/**
	 * Sends last message to server application and closes connection
	 */
	public void closeConnection() {
		String message = "close:1";
		packet = new DatagramPacket(message.getBytes(), message.getBytes().length, RPiAddress, PORT);
		stayAlive = false;
		try {
			socket.send(packet);
			socket.close();
			System.out.println("close sent");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Runs the thread to keep connection alive even if no messages are sent
	 * from controller
	 */
	@Override
	public void run() {
		while (stayAlive) {
			String message = "wake-up-message";
			packet = new DatagramPacket(message.getBytes(),message.getBytes().length, RPiAddress, PORT);
			try {
				socket.send(packet);
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}
