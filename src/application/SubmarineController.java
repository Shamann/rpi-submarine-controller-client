package application;
	
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;


public class SubmarineController extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader();
			fxmlLoader.setResources(ResourceBundle.getBundle("bundles.language", Locale.getDefault()));
			HBox root = (HBox)fxmlLoader.load(this.getClass().getResource("MainWindow.fxml").openStream());
			Scene scene = new Scene(root,700,120);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			MainWindowController controller = fxmlLoader.getController();
			controller.initializeAccelerators(scene);
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
				
				@Override
				public void handle(WindowEvent event) {
					controller.onExit();
				}
			});
			
			String title = fxmlLoader.getResources().getString("title");
			primaryStage.setTitle(title);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
