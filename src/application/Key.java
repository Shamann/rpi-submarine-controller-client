package application;

import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;

public class Key {
	private final KeyCode keyCode;
    private boolean pressed;
	private Button button;

	/**
	 * Binds the given button with keyboard Key and all events connected
	 * @param keyCode code of keyboard Key
	 * @param button javafx.scene.control.Button to be binded
	 */
    public Key(final KeyCode keyCode, final Button button) {
        this.keyCode = keyCode;
        this.pressed= false;
    	this.button = button;
    }

    public KeyCode getKeyCode() {
        return keyCode;
    }

    public boolean isPressed() {
        return pressed;
    }

    /**
	 * Changes appearance of button and fires action - if button is continuously
	 * being pressed fires action only once and returns false, otherwise returns
	 * true
	 * 
	 * @param value
	 * @return
	 */
    public boolean setPressed(final boolean value) {
        if (value && !pressed) {
        	button.arm();
        	button.fire();
        	pressed = value;
        	return true;
        } else if (pressed && !value) {
        	button.disarm();
        	button.fire();
        	pressed = false;
        	return true;
        }
        return false;
    }
    
    /**
     * For manual set of pressed attribute (use for mouse pressed/clicked/released)
     * @param value
     */
    public void manualPressed(final boolean value) {
    	pressed = value;
    }
}
